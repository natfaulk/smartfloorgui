const serialport = require('serialport')
const Mindrawing = require('mindrawingjs')
const interp2d = require('./interp.js').interp2d
const Mingraphing = require('mingraphing')

const NUM_ROWS = 4
const NUM_COLS = 4

const NUM_CAL_VALS = 10

let myApp = angular.module('myApp', [])
myApp.controller('display', ['$scope', '$interval', function($s, $interval) {
  $s.ports = []
  $s.portsConnected = []
  $s.portsConnectedObjs = []
  // $s.serialconnected = false
  $s.currentpage = 'main'
  $s.dataBuffer = []

  $s.comlist = []

  // $s.idMap = {
  //   0: 11,
  //   1: 7,
  //   2: 5,
  //   3: 6,
  //   4: 10,
  //   5: 8,
  //   6: 2,
  //   7: 4,
  //   8: 9,
  //   9: 1,
  //   10: 0,
  //   11: 3
  // }

  // $s.idMap = {
  //   0: 5,
  //   1: 6,
  //   2: 1,
  //   3: 2,
  //   4: 4,
  //   5: 7,
  //   6: 0,
  //   7: 3,
  //   8: 14,
  //   9: 15,
  //   10: 9,
  //   11: 10,
  //   12: 13,
  //   13: 12,
  //   14: 8,
  //   15: 11
  // }

  $s.idMap = {
    0: 13,
    1: 12,
    2: 8,
    3: 11,
    4: 14,
    5: 15,
    6: 9,
    7: 10,
    8: 4,
    9: 7,
    10: 0,
    11: 3,
    12: 5,
    13: 6,
    14: 1,
    15: 2
  }
  
  $s.settings = {
    disptext: true,
    dispgrid: true,
    dispcom: true,
    dispcomdraw: false,
    numCalVals: NUM_CAL_VALS,
    max: 1000000,
    interpNumber: 10,
    comSumThresh: 100000,
    loadcellGraphHistory: 100
  }

  $s.graph = new Mingraphing('loadcells', {
    chartHeight: 400,
    chartMargin: 10,
    numberOfCharts: 1,
    chartWidth: 1200,
    type: 'line',
    bipolar: false,
    drawMargins: true,
    tooltip: true,
    xMax: -1
  })

  $s.loadcells = []
  // for (let i = 0; i < 500; i++) {
  //   $s.loadcells.push({'loadcell0': Math.sin(i / 100)})
  // }

  for (let i = 0; i < NUM_COLS * NUM_ROWS; i++) {
    $s.graph.addData($s.loadcells, `loadcell${i}`, 'RED', 0, 3000000, `loadcell${i}`)
  }
  $s.graph.addData($s.loadcells, `sum`, 'BLUE', 0, 10000000, `Sum`)


  $s.maxval = -1
  $s.latestData = {}
  for (let i = 0; i < NUM_COLS * NUM_ROWS; i++) {
    let t = {}
    t.val = 0
    t.offset = 0
    // t.max = 50000
    t.calibrated = false
    t.calCount = 0
    $s.latestData[i.toString()] = t
  }
  
  $s.d = new Mindrawing()
  
  $s.d.setup('display')
  let rect = $s.d.c.parentNode.getBoundingClientRect()
  $s.d.setCanvasSize(rect.width, rect.width)

  $s.drawGraph = () => {
    let tempBuff = []
    // console.log($s.latestData)
    
    for (let r = 0; r < NUM_ROWS; r++) {
      tempBuff.push([])
      for (let c = 0; c < NUM_COLS; c++) {
        let c_to_id = $s.idMap[r * NUM_COLS + c]
        tempBuff[r].push($s.latestData[c_to_id].val)
      }
    }

    let tx = 0.5
    let ty = 0.5
    let sum = 0
    for (let i = 0; i < NUM_COLS * NUM_ROWS; i++) sum += $s.latestData[i].val

    for (let i_x = 0; i_x < NUM_COLS; i_x++) {
      for (let i_y = 0; i_y < NUM_ROWS; i_y++) {
        tx += i_x * tempBuff[i_y][i_x] / sum
        ty += i_y * tempBuff[i_y][i_x] / sum
      }
    }

    // console.log(tx, ty)

    // console.log(tempBuff)
    // console.log(interp2d(tempBuff, $s.settings.interpNumber))
    let intData = interp2d(tempBuff, $s.settings.interpNumber)

    $s.d.background('black')
    // draw squares
    let count = 0;
    let tWidth = $s.d.width / (intData[0].length - $s.settings.interpNumber)
    let tHeight = $s.d.height / (intData.length - $s.settings.interpNumber)
    let tOffsetX = $s.settings.interpNumber * tWidth / 2
    let tOffsetY = $s.settings.interpNumber * tHeight / 2
    for (let r = 0; r < intData.length; r++) {
      for (let c = 0; c < intData[0].length; c++) {
        // let t = Math.round(r * (128/NUM_ROWS) + c * (128/NUM_COLS))
        // console.log(t)
        // let c_to_id = $s.idMap[count]
        let t = Math.round((intData[r][c] / $s.settings.max) * 255)
        if (t < 0) t = 0
        if (t > 255) t = 255
        $s.d.stroke(`rgb(${t},${t},${t})`)
        $s.d.fill(`rgb(${t},${t},${t})`)
        $s.d.rect(Math.round(c * tWidth - tOffsetX) - 0.5, Math.round(r * tHeight - tOffsetY) - 0.5, tWidth + 1, tHeight + 1)
        // if ($s.settings.disptext) {
          //   $s.d.textSize(($s.d.height / NUM_ROWS) / 10)
          //   if (t > 127) t = 0
          //   else t = 255
          //   $s.d.fill(`rgb(${t},${t},${t})`)
          //   $s.d.text(`ID${c_to_id} = ${intData[r, c]}`, (c + 0.1) * tWidth, (r + 0.5) * tHeight)
          // }
        count++;
      }
    }
      
    if ($s.settings.dispgrid) {
      // draw grid
      $s.d.stroke('white')
      for (let i = 1; i < NUM_COLS; i++) {
        $s.d.line($s.d.width * i / NUM_COLS, 0, $s.d.width * i / NUM_COLS, $s.d.height)
      }
      
      for (let i = 1; i < NUM_ROWS; i++) {
        $s.d.line(0, $s.d.height * i / NUM_ROWS, $s.d.width, $s.d.height * i / NUM_ROWS)
      }
    }

    if ($s.settings.dispcom && sum > $s.settings.comSumThresh) {
      $s.d.fill(`rgb(255,0,0)`)
      $s.d.ellipse($s.d.width * tx / NUM_COLS, $s.d.width * ty / NUM_ROWS, 20)
    }

    if ($s.settings.dispcomdraw && sum > $s.settings.comSumThresh) {
      $s.d.fill(`rgb(255,0,0)`)

      $s.comlist.push([$s.d.width * tx / NUM_COLS, $s.d.width * ty / NUM_ROWS])
      $s.comlist.forEach(e => {
        $s.d.ellipse(e[0], e[1], 20)
      }) 
    } else $s.comlist = []

    $s.sumall = sum
  }
  $s.drawGraph()

  $s.refreshPorts = () => {
    serialport.list((err, _ports) => {
      if (err) console.log("Couldn't list ports")
      else {
        $s.ports = _ports
        console.log(_ports)
        $s.$apply()
      }
    })
  }

  $s.refreshPorts()

  $s.connectPort = (_port) => {
    $s.port = new serialport(_port, {baudRate: 115200})
    $s.port.on('open', () => {
      // $s.serialconnected = true
      $s.portsConnected.push(_port)
      $s.currentpage = 'display'
      $s.$apply()
      $s.port.dataBuffer = ''
    })

    $s.port.on('error', () => {
      console.log('There was a serial error');
    })

    $s.port.on('data', (data) => {
      $s.port.dataBuffer += data
      let tempIndex = $s.port.dataBuffer.indexOf('\n')
      if (tempIndex != -1) {
        let tempData = $s.port.dataBuffer.slice(0, tempIndex)
        if (tempData.startsWith("[Data] {")) {
          try {
            let parsedData = JSON.parse(tempData.slice(tempData.indexOf('{')))
            // console.log(Object.keys(parsedData).length)
            for (let i = 0; i < NUM_ROWS * NUM_COLS; i++) {
              // currently if the mega cannot read the device it returns a 0
              // should make it not add it to the json instead
              if (parsedData[i.toString()] == 0) console.log('glitch happened')
              else if (parsedData[i.toString()] !== undefined) {
                if ($s.latestData[i.toString()].calibrated) {
                  $s.latestData[i.toString()].val = Math.round(parsedData[i.toString()] - $s.latestData[i.toString()].offset) * -1
                } else {
                  $s.latestData[i.toString()].offset += parsedData[i.toString()] / $s.settings.numCalVals
                  $s.latestData[i.toString()].calCount++
                  if ($s.latestData[i.toString()].calCount >= $s.settings.numCalVals) {
                    $s.latestData[i.toString()].calibrated = true
                    $s.latestData[i.toString()].calCount = 0
                  }
                }
                $s.$apply()
              }
            }

            $s.drawGraph()
          } catch (e) {
            console.log('json parse failed')
            console.log(tempData.slice(tempData.indexOf('{')))
          }

          let tempMax = 0
          let tempObj = {}
          for (let i = 0; i < NUM_COLS * NUM_ROWS; i++) {
            if ($s.latestData[i].val > $s.latestData[tempMax].val) tempMax = i
            tempObj[`loadcell${i}`] = $s.latestData[i].val
          }
          tempObj['sum'] = $s.sumall
          $s.maxval = tempMax
          $s.loadcells.push(tempObj)
          while ($s.loadcells.length > $s.settings.loadcellGraphHistory) $s.loadcells.splice(0, 1)

          Object.keys(tempObj).forEach(e => {
            $s.graph.updateData($s.loadcells, e)
          })
        }
        $s.port.dataBuffer = $s.port.dataBuffer.slice(tempIndex + 1)
      }
    })

    $s.portsConnectedObjs.push($s.port)
  }

  $s.closePort = (_port) => {
    for (let i = 0; i < $s.portsConnectedObjs.length; i++) {
      if ($s.portsConnectedObjs[i].path == _port)
      {
        $s.portsConnectedObjs[i].close((err) => {
          if (err) console.log("Serial errror disconnecting")
          $s.portsConnectedObjs.splice(i, 1)
          $s.portsConnected.splice(i, 1)
          $s.$apply()
        })
      }
    }
  }

  $s.tare = () => {
    for (let i = 0; i < NUM_COLS * NUM_ROWS; i++) {
      $s.latestData[i.toString()].calibrated = false
      $s.latestData[i.toString()].calCount = 0
      $s.latestData[i.toString()].offset = 0
    }
  }
}])