module.exports = {
  interp2d: (_buff, nPoints) => {
    // pad with zeroes around the outside
    let _buff2 = []
    for (let i = 0; i < _buff.length + 2; i++) _buff2.push([0])
    for (let i = 0; i < _buff[0].length; i++) {
      _buff2[0].push(0)
      _buff2[_buff2.length - 1].push(0)
    }
    for (let i = 0; i < _buff.length; i++) {
      for (let j = 0; j < _buff[i].length; j++) {
        _buff2[i + 1].push(_buff[i][j])
      }
    }
    for (let i = 0; i < _buff.length + 2; i++) _buff2[i].push(0)

    _buff = _buff2

    let out = []
    for (let i = 0; i < _buff.length; i++) {
      out.push([])
      if (i < _buff.length - 1) for (let j = 0; j < nPoints; j++) {
        out.push([])
      }
    }

    out.forEach(element => {
      for (let i = 0; i < _buff[0].length + (_buff[0].length - 1) * nPoints; i++) element.push(0)
    })

    // console.log(out)

    for (let i = 0; i < _buff.length; i++) {
      for (let j = 0; j < _buff[0].length; j++) {
        out[i * (nPoints + 1)][j * (nPoints + 1)] = _buff[i][j]
      }
    }

    // output filled with all known values
    // now interpolate across
    for (let i = 0; i < _buff.length; i++) {
      for (let j = 0; j < _buff[0].length - 1; j++) {
        for (let k = 1; k <= nPoints; k++) {
          let tY = i * (nPoints + 1)
          let tX = j * (nPoints + 1)
          let intVal = out[tY][tX] + k * ((out[tY][tX + nPoints + 1] - out[tY][tX]) / (nPoints + 1))
          out[tY][tX + k] = intVal
        }
      }
    }

    // now interpolate down
    for (let j = 0; j < out[0].length; j++) {
      for (let i = 0; i < _buff.length - 1; i++) {
        for (let k = 1; k <= nPoints; k++) {
          let tY = i * (nPoints + 1)
          let tX = j
          // console.log(out[tY][tX])
          // console.log(out[tY][tX + nPoints + 1])
          // console.log(k)
          let intVal = out[tY][tX] + k * ((out[tY + nPoints + 1][tX] - out[tY][tX]) / (nPoints + 1))
          // console.log(intVal)
          out[tY + k][tX] = intVal
        }
      }
    }

    // console.log(out)

    return out
  }
}