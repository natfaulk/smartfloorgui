#include <ArduinoJson.h>

void setup(void)
{
  Serial.begin(115200);
  Serial.println("[Info] Setup done");

  // send calibration vals:
  for (int i = 0; i < 15; i++) {
    StaticJsonBuffer<500> jsonBuffer;
    JsonObject& root = jsonBuffer.createObject();
    for (uint8_t i = 0; i < 12; i++)
    {
      root[String(i)] = 255;
    }

    Serial.print("[Data] ");
    root.printTo(Serial);
    Serial.println();
    delay(1000);
  }
}

void loop(void)
{
  StaticJsonBuffer<500> jsonBuffer;

  JsonObject& root = jsonBuffer.createObject();
  for (uint8_t i = 0; i < 12; i++)
  {
    root[String(i)] = random(256);
  }

  Serial.print("[Data] ");
  root.printTo(Serial);
  Serial.println();
  delay(1000);
}